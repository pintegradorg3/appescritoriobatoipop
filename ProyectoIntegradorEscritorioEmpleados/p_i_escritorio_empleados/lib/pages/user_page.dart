import 'package:flutter/material.dart';
import 'package:p_i_escritorio_empleados/pages/profile_page.dart';

import 'stats_page.dart';
import 'category_page.dart';
import 'complaints_page.dart';

class UserPage extends StatefulWidget {
  @override
  State<UserPage> createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
            child: Container(
                color: const Color(0xAADDDDFF),
                child: ListView(children: [
                  SizedBox(
                      height: 55,
                      child: DrawerHeader(
                          child: Container(
                              child: const Text("Usuarios",
                                  style: TextStyle(fontSize: 20)),
                              alignment: Alignment.bottomLeft))),
                  _DrawerTile(
                      current: false, img: "tag.png", text: "Categorias"),
                  _DrawerTile(
                      current: true, img: "users.png", text: "Usuarios"),
                  _DrawerTile(
                      current: false, img: "stats.png", text: "Estadisticas"),
                  _DrawerTile(
                      current: false, img: "warning2.png", text: "Denuncias"),
                ]))),
        appBar: AppBar(
          backgroundColor: const Color(0xFFDDDDFF),
          title: Row(children: [
            Image.asset("logo_completo.png", height: 50, width: 150),
            const Spacer(flex: 100),
            Image.asset("message.png", height: 30),
            const Spacer(),
            const Text("Mensajes", style: TextStyle(color: Colors.black)),
            const Spacer(flex: 3),
            GestureDetector(
              child: Image.asset("user.png", height: 50, width: 50),
              onTap: () {
                var route =
                    MaterialPageRoute(builder: (context) => ProfilePage());
                Navigator.of(context).push(route);
              },
            )
          ]),
        ),
        body: Container(
          child: _Body(),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("logo_completo.png"),
                  colorFilter: ColorFilter.mode(
                      Colors.white.withOpacity(0.4), BlendMode.dstATop))),
        ));
  }

// Future<http.Response> fetchAlbum() {
//   return http.get(Uri.parse('http://localhost:8080/usuarios'));
// }
}

class _DrawerTile extends StatefulWidget {
  final bool current;
  final String img;
  final String text;

  _DrawerTile({required this.current, required this.img, required this.text});

  @override
  State<_DrawerTile> createState() => _DrawerTileState();
}

class _DrawerTileState extends State<_DrawerTile> {
  @override
  Widget build(BuildContext context) {
    Color tileColor = Color(0xAADDDDFF);
    if (widget.current) {
      tileColor = Color(0xFF9999FF);
    }

    var categoryRoute;
    switch (widget.text) {
      case "Categorias":
        categoryRoute = MaterialPageRoute(builder: (context) => CategoryPage());
        break;
      case "Usuarios":
        categoryRoute = MaterialPageRoute(builder: (context) => UserPage());
        break;
      case "Estadisticas":
        categoryRoute = MaterialPageRoute(builder: (context) => StatsPage());
        break;
      case "Denuncias":
        categoryRoute =
            MaterialPageRoute(builder: (context) => ComplaintsPage());
        break;
    }

    return Material(
      child: ListTile(
        enabled: true,
        tileColor: tileColor,
        hoverColor: Color(0xAAAAAAFF),
        title: Row(
          children: [
            Image.asset(
              widget.img,
              height: 25,
              width: 25,
            ),
            const Spacer(),
            Text(widget.text),
            const Spacer(flex: 40)
          ],
        ),
        onTap: () {
          Navigator.of(context).push(categoryRoute);
        },
      ),
    );
  }
}

class _Body extends StatefulWidget {
  @override
  State<_Body> createState() => _BodyState();
}

class _BodyState extends State<_Body> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(
      children: [
        Row(
          children: [
            Spacer(),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
              padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
              height: 40,
              width: 500,
              decoration: BoxDecoration(
                  color: Color(0x660000aa),
                  borderRadius: BorderRadius.circular(90)),
              child: Row(
                children: [
                  Image.asset(
                    "search.png",
                    height: 20,
                  ),
                  Spacer(flex: 5),
                  Text("Buscar informacion"),
                  Spacer(flex: 90)
                ],
              ),
            ),
          ],
        ),
        Spacer(),
        Container(
          alignment: Alignment.center,
          child: Row(
            children: [
              Spacer(),
              _UserColumn(
                  title: "ID",
                  element1: "1",
                  element2: "2",
                  element3: "3",
                  element4: "4",
                  element5: "5",
                  element6: "6",
                  element7: "7",
                  element8: "8"),
              _UserColumn(
                  title: "NOMBRE",
                  element1: "Alfonso",
                  element2: "Alicia",
                  element3: "Valentin",
                  element4: "Juan",
                  element5: "Cristina",
                  element6: "Nerea",
                  element7: "Pedro",
                  element8: "Maria"),
              _UserColumn(
                  title: "APELLIDOS",
                  element1: "Ferrando Ruiz",
                  element2: "Perez Jorda",
                  element3: "Morales Mora",
                  element4: "Dolores Miralles",
                  element5: "Monllor Aznar",
                  element6: "Garces Oliver",
                  element7: "Milano Ribelles",
                  element8: "Ferre Cornera"),
              _UserColumn(
                  title: "CORREO",
                  element1: "alferrando@gmail.com",
                  element2: "aliperez@gmail.com",
                  element3: "valentimo@gmail.com",
                  element4: "juandomi@gmail.com",
                  element5: "cristi33@gmail.com",
                  element6: "neregarco@gmail.com",
                  element7: "pedrito49@gmail.com",
                  element8: "marife@gmail.com"),
              _UserColumn(
                  title: "PRODUCTOS AÑADIDOS",
                  element1: "7",
                  element2: "4",
                  element3: "5",
                  element4: "6",
                  element5: "2",
                  element6: "10",
                  element7: "0",
                  element8: "5"),
              _UserColumn(
                  title: "PRODUCTOS VENDIDOS",
                  element1: "3",
                  element2: "2",
                  element3: "4",
                  element4: "1",
                  element5: "0",
                  element6: "5",
                  element7: "0",
                  element8: "3"),
              _UserColumn(
                  title: "DIRECCION",
                  element1: "C/ Entenza Nº40",
                  element2: "C/ Santa Rosa Nº23",
                  element3: "C/ Peru Nº18",
                  element4: "C/ Tossal Nº24",
                  element5: "C/ San Sebastian Nº56",
                  element6: "C/ Mestre Espi Nº15",
                  element7: "C/ Oliver Nº13",
                  element8: "C/ La Hispanidad Nº32"),
              Spacer()
            ],
          ),
        ),
        Spacer()
      ],
    ));
  }
}

class _UserColumn extends StatelessWidget {
  final String title;
  final String element1;
  final String element2;
  final String element3;
  final String element4;
  final String element5;
  final String element6;
  final String element7;
  final String element8;

  const _UserColumn(
      {required this.title,
      required this.element1,
      required this.element2,
      required this.element3,
      required this.element4,
      required this.element5,
      required this.element6,
      required this.element7,
      required this.element8});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 600,
      child: Column(
        children: [
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 200,
              child: Text(title),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xFFDDDDFF)),
                  color: Color(0x660000AA))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 200,
              child: Text(element1),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 200,
              child: Text(element2),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 200,
              child: Text(element3),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 200,
              child: Text(element4),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 200,
              child: Text(element5),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 200,
              child: Text(element6),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 200,
              child: Text(element7),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 200,
              child: Text(element8),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
        ],
      ),
    );
  }
}
