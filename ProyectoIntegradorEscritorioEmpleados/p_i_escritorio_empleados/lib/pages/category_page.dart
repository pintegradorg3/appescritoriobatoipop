import 'package:flutter/material.dart';
import 'package:p_i_escritorio_empleados/pages/profile_page.dart';

import 'complaints_page.dart';
import 'stats_page.dart';
import 'user_page.dart';

class CategoryPage extends StatefulWidget {
  @override
  State<CategoryPage> createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
            child: Container(
                color: const Color(0xAADDDDFF),
                child: ListView(children: [
                  SizedBox(
                      height: 55,
                      child: DrawerHeader(
                          child: Container(
                              child: const Text("Categorias",
                                  style: TextStyle(fontSize: 20)),
                              alignment: Alignment.bottomLeft))),
                  _DrawerTile(
                      current: true, img: "tag.png", text: "Categorias"),
                  _DrawerTile(
                      current: false, img: "users.png", text: "Usuarios"),
                  _DrawerTile(
                      current: false, img: "stats.png", text: "Estadisticas"),
                  _DrawerTile(
                      current: false, img: "warning2.png", text: "Denuncias"),
                ]))),
        appBar: AppBar(
          backgroundColor: const Color(0xFFDDDDFF),
          title: Row(children: [
            Image.asset("logo_completo.png", height: 50, width: 150),
            const Spacer(flex: 100),
            Image.asset("message.png", height: 30),
            const Spacer(),
            const Text("Mensajes", style: TextStyle(color: Colors.black)),
            const Spacer(flex: 3),
            GestureDetector(
                child: Image.asset("user.png", height: 50, width: 50),
                onTap: () {
                  var route =
                      MaterialPageRoute(builder: (context) => ProfilePage());
                  Navigator.of(context).push(route);
                })
          ]),
        ),
        body: Container(
          child: _Body(),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("logo_completo.png"),
                  colorFilter: ColorFilter.mode(
                      Colors.white.withOpacity(0.4), BlendMode.dstATop))),
        ));
  }
}

class _DrawerTile extends StatefulWidget {
  final bool current;
  final String img;
  final String text;

  _DrawerTile({required this.current, required this.img, required this.text});

  @override
  State<_DrawerTile> createState() => _DrawerTileState();
}

class _DrawerTileState extends State<_DrawerTile> {
  @override
  Widget build(BuildContext context) {
    Color tileColor = Color(0xAADDDDFF);
    if (widget.current) {
      tileColor = Color(0xFF9999FF);
    }

    var categoryRoute;
    switch (widget.text) {
      case "Categorias":
        categoryRoute = MaterialPageRoute(builder: (context) => CategoryPage());
        break;
      case "Usuarios":
        categoryRoute = MaterialPageRoute(builder: (context) => UserPage());
        break;
      case "Estadisticas":
        categoryRoute = MaterialPageRoute(builder: (context) => StatsPage());
        break;
      case "Denuncias":
        categoryRoute =
            MaterialPageRoute(builder: (context) => ComplaintsPage());
        break;
    }

    return Material(
      child: ListTile(
        enabled: true,
        tileColor: tileColor,
        hoverColor: Color(0xAAAAAAFF),
        title: Row(
          children: [
            Image.asset(
              widget.img,
              height: 25,
              width: 25,
            ),
            const Spacer(),
            Text(widget.text),
            const Spacer(flex: 40)
          ],
        ),
        onTap: () {
          Navigator.of(context).push(categoryRoute);
        },
      ),
    );
  }
}

class _Body extends StatefulWidget {
  @override
  State<_Body> createState() => _BodyState();
}

class _BodyState extends State<_Body> {
  String selectedCategory = "Mascotas";
  var categories = ['Mascotas', 'Vehiculos', 'Hogar', 'Ropa', 'Deportes'];

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(
      children: [
        Row(
          children: [
            Spacer(),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
              padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
              height: 40,
              width: 500,
              decoration: BoxDecoration(
                  color: Color(0x660000aa),
                  borderRadius: BorderRadius.circular(90)),
              child: Row(
                children: [
                  Image.asset(
                    "search.png",
                    height: 20,
                  ),
                  Spacer(flex: 5),
                  Text("Buscar informacion"),
                  Spacer(flex: 90)
                ],
              ),
            )
          ],
        ),
        Spacer(),
        Row(
          children: [
            Spacer(flex: 5),
            Container(
              height: 400,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Spacer(flex: 2),
                  Text("Elegir categoria: ", style: TextStyle(fontSize: 30)),
                  Spacer(),
                  Text("Imagen: ", style: TextStyle(fontSize: 30)),
                  Spacer(flex: 1),
                  Text("Nombre: ", style: TextStyle(fontSize: 30)),
                  Spacer()
                ],
              ),
            ),
            Spacer(),
            Container(
              height: 400,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Spacer(flex: 2),
                  Container(
                    padding: EdgeInsets.fromLTRB(5, 0, 5, 5),
                    decoration: BoxDecoration(
                        color: Color(0xFFCCCCCC),
                        border: Border.all(color: Colors.black),
                        borderRadius: BorderRadius.circular(10)),
                    child: DropdownButton<String>(
                      value: selectedCategory,
                      icon: const Icon(Icons.arrow_downward),
                      elevation: 16,
                      style: const TextStyle(color: Colors.black),
                      underline: Container(
                        height: 2,
                        color: Colors.deepPurpleAccent,
                      ),
                      onChanged: (String? newValue) {
                        setState(() {
                          selectedCategory = newValue!;
                        });
                      },
                      items: categories
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                  Spacer(),
                  Image.asset(selectedCategory.toString() + ".png", height: 80),
                  Spacer(flex: 1),
                  Text(selectedCategory.toString(),
                      style: TextStyle(fontSize: 25, color: Color(0xFF777777))),
                  Spacer(),
                ],
              ),
            ),
            Spacer(flex: 5)
          ],
        ),
        Spacer(),
        Row(
          children: [
            Spacer(),
            GestureDetector(
                child: Container(
                  width: 400,
                  height: 50,
                  decoration: BoxDecoration(
                    color: Colors.red,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: const Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Eliminar categoria",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
                onTap: () {
                  for (int i = 0; i < categories.length; i++) {
                    if (selectedCategory == categories.elementAt(i)) {
                      categories.removeAt(i);
                    }
                  }
                }),
            Spacer(),
            GestureDetector(
                child: Container(
                  width: 400,
                  height: 50,
                  decoration: BoxDecoration(
                    color: const Color(0xFF5A2CA8),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: const Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Añadir categoria",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
                onTap: () {
                  categories.add("Categoria" + categories.length.toString());
                }),
            Spacer()
          ],
        ),
        Spacer()
      ],
    ));
  }
}
