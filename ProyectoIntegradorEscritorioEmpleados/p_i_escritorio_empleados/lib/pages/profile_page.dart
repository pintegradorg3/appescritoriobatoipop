import 'package:flutter/material.dart';
import 'package:p_i_escritorio_empleados/pages/login_page.dart';
import 'package:p_i_escritorio_empleados/pages/stats_page.dart';
import 'package:p_i_escritorio_empleados/pages/user_page.dart';

import 'category_page.dart';
import 'complaints_page.dart';

class ProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CategoryPageState();
}

class _CategoryPageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
            child: Container(
                color: const Color(0xAADDDDFF),
                child: ListView(children: [
                  SizedBox(
                      height: 55,
                      child: DrawerHeader(
                          child: Container(
                              child: const Text("Perfil",
                                  style: TextStyle(fontSize: 20)),
                              alignment: Alignment.bottomLeft))),
                  _DrawerTile(
                      current: false, img: "tag.png", text: "Categorias"),
                  _DrawerTile(
                      current: false, img: "users.png", text: "Usuarios"),
                  _DrawerTile(
                      current: false, img: "stats.png", text: "Estadisticas"),
                  _DrawerTile(
                      current: false, img: "warning2.png", text: "Denuncias"),
                ]))),
        appBar: AppBar(
          backgroundColor: const Color(0xFFDDDDFF),
          title: Row(children: [
            Image.asset("logo_completo.png", height: 50, width: 150),
            const Spacer(flex: 100),
            Image.asset("message.png", height: 30),
            const Spacer(),
            const Text("Mensajes", style: TextStyle(color: Colors.black)),
            const Spacer(flex: 3),
            GestureDetector(
              child: Image.asset("user.png", height: 50, width: 50),
              onTap: () {
                var route =
                    MaterialPageRoute(builder: (context) => ProfilePage());
                Navigator.of(context).push(route);
              },
            )
          ]),
        ),
        body: Container(
          child: _Body(),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("logo_completo.png"),
                  colorFilter: ColorFilter.mode(
                      Colors.white.withOpacity(0.4), BlendMode.dstATop))),
        ));
  }
}

class _DrawerTile extends StatefulWidget {
  final bool current;
  final String img;
  final String text;

  _DrawerTile({required this.current, required this.img, required this.text});

  @override
  State<_DrawerTile> createState() => _DrawerTileState();
}

class _DrawerTileState extends State<_DrawerTile> {
  @override
  Widget build(BuildContext context) {
    Color tileColor = Color(0xAADDDDFF);
    if (widget.current) {
      tileColor = Color(0xFF9999FF);
    }

    var categoryRoute;
    switch (widget.text) {
      case "Categorias":
        categoryRoute = MaterialPageRoute(builder: (context) => CategoryPage());
        break;
      case "Usuarios":
        categoryRoute = MaterialPageRoute(builder: (context) => UserPage());
        break;
      case "Estadisticas":
        categoryRoute = MaterialPageRoute(builder: (context) => StatsPage());
        break;
      case "Denuncias":
        categoryRoute =
            MaterialPageRoute(builder: (context) => ComplaintsPage());
        break;
    }

    return Material(
      child: ListTile(
        enabled: true,
        tileColor: tileColor,
        hoverColor: Color(0xAAAAAAFF),
        title: Row(
          children: [
            Image.asset(
              widget.img,
              height: 25,
              width: 25,
            ),
            const Spacer(),
            Text(widget.text),
            const Spacer(flex: 40)
          ],
        ),
        onTap: () {
          Navigator.of(context).push(categoryRoute);
        },
      ),
    );
  }
}

class _Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(
      children: [
        Row(
          children: [
            Spacer(),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
              padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
              height: 40,
              width: 500,
              decoration: BoxDecoration(
                  color: Color(0x660000aa),
                  borderRadius: BorderRadius.circular(90)),
              child: Row(
                children: [
                  Image.asset(
                    "search.png",
                    height: 20,
                  ),
                  Spacer(flex: 5),
                  Text("Buscar informacion"),
                  Spacer(flex: 90)
                ],
              ),
            )
          ],
        ),
        Spacer(),
        Row(children: [
          Spacer(),
          Text("Administrador",
              style: TextStyle(fontSize: 30, color: Color(0xFF777777))),
          Spacer(flex: 5)
        ]),
        Spacer(flex: 2),
        Row(
          children: [
            Container(
              height: 400,
              margin: EdgeInsets.fromLTRB(50, 0, 50, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Spacer(),
                  Text("Nombre", style: TextStyle(fontSize: 20)),
                  Spacer(),
                  Text("Apellidos", style: TextStyle(fontSize: 20)),
                  Spacer(flex: 1),
                  Text("Direccion de correo", style: TextStyle(fontSize: 20)),
                ],
              ),
            ),
            Spacer(),
            Container(
              height: 400,
              margin: EdgeInsets.fromLTRB(50, 0, 50, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Spacer(),
                  Text("Alex",
                      style: TextStyle(fontSize: 20, color: Color(0xFF777777))),
                  Spacer(),
                  Text("Martinez Parra",
                      style: TextStyle(fontSize: 20, color: Color(0xFF777777))),
                  Spacer(flex: 1),
                  Text("alexmp@gmail.com",
                      style: TextStyle(fontSize: 20, color: Color(0xFF777777))),
                ],
              ),
            ),
            Spacer(),
            Container(
              height: 400,
              margin: EdgeInsets.fromLTRB(50, 0, 50, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Spacer(),
                  Text("Idioma", style: TextStyle(fontSize: 20)),
                  Spacer(flex: 1),
                  Text("Zona horaria", style: TextStyle(fontSize: 20)),
                  Spacer(flex: 1)
                ],
              ),
            ),
            Spacer(),
            Container(
              height: 400,
              margin: EdgeInsets.fromLTRB(50, 0, 50, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Spacer(),
                  Text("Español",
                      style: TextStyle(fontSize: 20, color: Color(0xFF777777))),
                  Spacer(flex: 1),
                  Text("Europa Central",
                      style: TextStyle(fontSize: 20, color: Color(0xFF777777))),
                  Spacer(flex: 1)
                ],
              ),
            ),
            Spacer(flex: 3),
            Container(
              height: 400,
              child: Column(
                children: [
                  Spacer(),
                  Image.asset("user.png", height: 180),
                  Spacer()
                ],
              ),
            ),
            Spacer(flex: 5),
          ],
        ),
        Spacer(flex: 4),
        Row(
          children: [
            Spacer(),
            GestureDetector(
                child: Container(
                  width: 400,
                  height: 50,
                  decoration: BoxDecoration(
                    color: const Color(0xFF5A2CA8),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: const Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Cerrar sesion",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
                onTap: () {
                  var route =
                      MaterialPageRoute(builder: (context) => LoginPage());
                  Navigator.of(context).push(route);
                }),
            Spacer()
          ],
        ),
        Spacer()
      ],
    ));
  }
}
