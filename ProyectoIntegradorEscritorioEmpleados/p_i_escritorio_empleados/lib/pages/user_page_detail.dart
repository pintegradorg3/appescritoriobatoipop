// import 'package:flutter/material.dart';
//
// class UserPageDetail extends StatefulWidget {
//   String dId, dName, dSurnames, dEmail, dDireccion;
//
//   UserPageDetail(
//       {this.dName, this.dEmail, this.dId, this.dSurnames, this.dDireccion});
//
//   @override
//   _UserPageDetailState createState() => _UserPageDetailState();
// }
//
// class _UserPageDetailState extends State<UserPageDetail> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Detail User'),
//         backgroundColor: Colors.blue,
//       ),
//       body: ListView(
//         children: <Widget>[
//           Container(
//             padding: EdgeInsets.all(16),
//             child: Card(
//               child: Padding(
//                 padding: EdgeInsets.all(8),
//                 child: Column(
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   children: <Widget>[
//                     Container(
//                       child: Text(
//                         'With One Class',
//                         style: TextStyle(
//                             fontSize: 20, fontWeight: FontWeight.bold),
//                       ),
//                     ),
//                     Container(
//                       child: Column(
//                         children: <Widget>[
//                           Text(
//                             "${widget.dName}",
//                             style: TextStyle(
//                                 fontSize: 24,
//                                 color: Colors.green,
//                                 fontWeight: FontWeight.bold),
//                           ),
//                           Text(
//                             "Email : ${widget.dEmail}",
//                             style: TextStyle(fontSize: 16),
//                           ),
//                           Text(
//                             "Id : ${widget.dId}",
//                             style: TextStyle(fontSize: 16),
//                           ),
//                           Text(
//                             "Direccion : ${widget.dDireccion}",
//                             style: TextStyle(fontSize: 16),
//                           ),
//                           Text(
//                             "Apellidos : ${widget.dSurnames}",
//                             style: TextStyle(fontSize: 16),
//                           ),
//                         ],
//                       ),
//                     )
//                   ],
//                 ),
//               ),
//             ),
//           ),
//           SizedBox(
//             height: 40,
//           ),
//           Container(
//             child: Column(
//               children: <Widget>[
//                 new NameDetail(
//                   name: widget.dName,
//                   email: widget.dEmail,
//                 ),
//                 new BagianIcon(),
//                 new BagianContact(
//                   id: widget.dId,
//                   name: widget.dName,
//                   surname: widget.dSurnames,
//                 )
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
//
// class NameDetail extends StatelessWidget {
//   final String name, email;
//   NameDetail({this.name, this.email});
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return Container(
//       padding: EdgeInsets.all(16),
//       child: Row(
//         children: <Widget>[
//           Expanded(
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: <Widget>[
//                 Text(
//                   name,
//                   style: TextStyle(
//                       fontWeight: FontWeight.bold,
//                       color: Colors.green,
//                       fontSize: 24),
//                 ),
//                 Text(
//                   "Email : $email",
//                   style: TextStyle(fontSize: 16),
//                 ),
//               ],
//             ),
//           ),
//           Row(
//             children: <Widget>[
//               Icon(
//                 Icons.star,
//                 size: 40,
//                 color: Colors.orange,
//               ),
//               Text(
//                 "12",
//                 style: TextStyle(fontSize: 12),
//               )
//             ],
//           )
//         ],
//       ),
//     );
//   }
// }
//
// class BagianIcon extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.all(10),
//       child: Row(
//         children: <Widget>[
//           IconText(
//             iconData: Icons.camera,
//             text: "Camera",
//           ),
//           IconText(
//             iconData: Icons.phone,
//             text: "Phone",
//           ),
//           IconText(
//             iconData: Icons.message,
//             text: "Message",
//           ),
//         ],
//       ),
//     );
//   }
// }
//
// class IconText extends StatelessWidget {
//   IconText({this.iconData, this.text});
//   final IconData iconData;
//   final String text;
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return Expanded(
//       child: Column(
//         children: <Widget>[
//           Icon(
//             iconData,
//             size: 20,
//             color: Colors.green,
//           ),
//           Text(
//             text,
//             style: TextStyle(fontSize: 12, color: Colors.green),
//           ),
//         ],
//       ),
//     );
//   }
// }
//
// class BagianContact extends StatelessWidget {
//   String id, name, surname;
//
//   BagianContact({this.id, this.name, this.surname});
//
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return Container(
//       padding: EdgeInsets.all(16),
//       child: Card(
//         child: Padding(
//           padding: EdgeInsets.all(16),
//           child: Column(
//             children: <Widget>[
//               Container(
//                 child: Text(
//                   'With many Class',
//                   style: TextStyle(fontSize: 16),
//                 ),
//               ),
//               Container(
//                 child: Text(
//                   id,
//                   style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
//                 ),
//               ),
//               Container(
//                 child: Text(
//                   name,
//                   style: TextStyle(fontSize: 16),
//                 ),
//               ),
//               Container(
//                 child: Text(
//                   surname,
//                   style: TextStyle(fontSize: 16),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
