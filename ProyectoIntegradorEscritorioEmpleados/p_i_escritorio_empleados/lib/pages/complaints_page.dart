import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:p_i_escritorio_empleados/pages/profile_page.dart';

import 'stats_page.dart';
import 'user_page.dart';
import 'category_page.dart';

class ComplaintsPage extends StatefulWidget {
  @override
  State<ComplaintsPage> createState() => _ComplaintsPageState();
}

class _ComplaintsPageState extends State<ComplaintsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
            child: Container(
                color: const Color(0xAADDDDFF),
                child: ListView(children: [
                  SizedBox(
                      height: 55,
                      child: DrawerHeader(
                          child: Container(
                              child: const Text("Denuncias",
                                  style: TextStyle(fontSize: 20)),
                              alignment: Alignment.bottomLeft))),
                  _DrawerTile(
                      current: false, img: "tag.png", text: "Categorias"),
                  _DrawerTile(
                      current: false, img: "users.png", text: "Usuarios"),
                  _DrawerTile(
                      current: false, img: "stats.png", text: "Estadisticas"),
                  _DrawerTile(
                      current: true, img: "warning2.png", text: "Denuncias"),
                ]))),
        appBar: AppBar(
          backgroundColor: const Color(0xFFDDDDFF),
          title: Row(children: [
            Image.asset("logo_completo.png", height: 50, width: 150),
            const Spacer(flex: 100),
            Image.asset("message.png", height: 30),
            const Spacer(),
            const Text("Mensajes", style: TextStyle(color: Colors.black)),
            const Spacer(flex: 3),
            GestureDetector(
              child: Image.asset("user.png", height: 50, width: 50),
              onTap: () {
                var route =
                    MaterialPageRoute(builder: (context) => ProfilePage());
                Navigator.of(context).push(route);
              },
            )
          ]),
        ),
        body: Container(
          child: _Body(),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("logo_completo.png"),
                  colorFilter: ColorFilter.mode(
                      Colors.white.withOpacity(0.4), BlendMode.dstATop))),
        ));
  }
}

class _DrawerTile extends StatefulWidget {
  final bool current;
  final String img;
  final String text;

  _DrawerTile({required this.current, required this.img, required this.text});

  @override
  State<_DrawerTile> createState() => _DrawerTileState();
}

class _DrawerTileState extends State<_DrawerTile> {
  @override
  Widget build(BuildContext context) {
    Color tileColor = Color(0xAADDDDFF);
    if (widget.current) {
      tileColor = Color(0xFF9999FF);
    }

    var categoryRoute;
    switch (widget.text) {
      case "Categorias":
        categoryRoute = MaterialPageRoute(builder: (context) => CategoryPage());
        break;
      case "Usuarios":
        categoryRoute = MaterialPageRoute(builder: (context) => UserPage());
        break;
      case "Estadisticas":
        categoryRoute = MaterialPageRoute(builder: (context) => StatsPage());
        break;
      case "Denuncias":
        categoryRoute =
            MaterialPageRoute(builder: (context) => ComplaintsPage());
        break;
    }

    return Material(
      child: ListTile(
        enabled: true,
        tileColor: tileColor,
        hoverColor: Color(0xAAAAAAFF),
        title: Row(
          children: [
            Image.asset(
              widget.img,
              height: 25,
              width: 25,
            ),
            const Spacer(),
            Text(widget.text),
            const Spacer(flex: 40)
          ],
        ),
        onTap: () {
          Navigator.of(context).push(categoryRoute);
        },
      ),
    );
  }
}

class _Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(
      children: [
        Row(
          children: [
            Spacer(),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
              padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
              height: 40,
              width: 500,
              decoration: BoxDecoration(
                  color: Color(0x660000AA),
                  borderRadius: BorderRadius.circular(90)),
              child: Row(
                children: [
                  Image.asset(
                    "search.png",
                    height: 20,
                  ),
                  Spacer(flex: 5),
                  Text("Buscar informacion"),
                  Spacer(flex: 90)
                ],
              ),
            ),
          ],
        ),
        Spacer(
          flex: 10,
        ),
        Container(
          alignment: Alignment.center,
          child: Row(
            children: [
              Spacer(),
              _ComplaintColumn(
                  title: "ID",
                  element1: "1",
                  element2: "2",
                  element3: "3",
                  element4: "4",
                  element5: "5",
                  element6: "6",
                  element7: "7",
                  element8: "8"),
              _ComplaintColumn(
                  title: "USUARIO",
                  element1: "El Pere",
                  element2: "AliciaMaravillas",
                  element3: "Cristi33",
                  element4: "AliciaMaravillas",
                  element5: "Marcosete",
                  element6: "Nereida",
                  element7: "Juan",
                  element8: "AliciaMaravillas"),
              _ComplaintColumn(
                  title: "TIPO",
                  element1: "Producto",
                  element2: "Mensaje",
                  element3: "Producto",
                  element4: "Producto",
                  element5: "Mensaje",
                  element6: "Producto",
                  element7: "Mensaje",
                  element8: "Mensaje"),
              _ComplaintColumn(
                  title: "CONTENIDO",
                  element1: "Cuadro renacentista",
                  element2: "No puedo bajar de mas de 200",
                  element3: "Declaracion de la independencia de USA",
                  element4: "Libro Luna de Pluton",
                  element5:
                      "Mira chaval, como no me lo traigas descubrire donde vives",
                  element6: "Bici nueva",
                  element7: "Si que te voy a dar yo a ti",
                  element8: "Eso te pasa por no escuchar"),
              _ComplaintColumn(
                  title: "OFENSA",
                  element1: "La imagen de este cuadro no es muy adecuada",
                  element2: "Este precio es totalmente indignante",
                  element3: "Obviamente una falsificacion",
                  element4:
                      "El contenido de este libro es muy fuerte para el publico",
                  element5: "Me siento inseguro",
                  element6: "La bici es mia, me la robaron hace poco",
                  element7: "Me estan amenazando",
                  element8: "Claro atentado contra mi persona"),
              Spacer()
            ],
          ),
        ),
        Spacer()
      ],
    ));
  }
}

class _ComplaintColumn extends StatelessWidget {
  final String title;
  final String element1;
  final String element2;
  final String element3;
  final String element4;
  final String element5;
  final String element6;
  final String element7;
  final String element8;

  const _ComplaintColumn(
      {required this.title,
      required this.element1,
      required this.element2,
      required this.element3,
      required this.element4,
      required this.element5,
      required this.element6,
      required this.element7,
      required this.element8});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 600,
      child: Column(
        children: [
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 250,
              child: Text(title),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xFFDDDDFF)),
                  color: Color(0x660000AA))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 250,
              child: Text(element1),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 250,
              child: Text(element2),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 250,
              child: Text(element3),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 250,
              child: Text(element4),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 250,
              child: Text(element5),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 250,
              child: Text(element6),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 250,
              child: Text(element7),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              height: 50,
              width: 250,
              child: Text(element8),
              decoration: BoxDecoration(
                  border: Border.all(color: Color(0xAADDDDFF)),
                  color: Color(0xCCDDDDFF))),
        ],
      ),
    );
  }
}
