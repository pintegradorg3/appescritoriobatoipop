import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:p_i_escritorio_empleados/pages/profile_page.dart';

import 'user_page.dart';
import 'category_page.dart';
import 'complaints_page.dart';

class StatsPage extends StatefulWidget {
  @override
  State<StatsPage> createState() => _StatsPageState();
}

class _StatsPageState extends State<StatsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
            child: Container(
                color: const Color(0xAADDDDFF),
                child: ListView(children: [
                  SizedBox(
                      height: 55,
                      child: DrawerHeader(
                          child: Container(
                              child: const Text("Estadisticas",
                                  style: TextStyle(fontSize: 20)),
                              alignment: Alignment.bottomLeft))),
                  _DrawerTile(
                      current: false, img: "tag.png", text: "Categorias"),
                  _DrawerTile(
                      current: false, img: "users.png", text: "Usuarios"),
                  _DrawerTile(
                      current: true, img: "stats.png", text: "Estadisticas"),
                  _DrawerTile(
                      current: false, img: "warning2.png", text: "Denuncias"),
                ]))),
        appBar: AppBar(
          backgroundColor: const Color(0xFFDDDDFF),
          title: Row(children: [
            Image.asset("logo_completo.png", height: 50, width: 150),
            const Spacer(flex: 100),
            Image.asset("message.png", height: 30),
            const Spacer(),
            const Text("Mensajes", style: TextStyle(color: Colors.black)),
            const Spacer(flex: 3),
            GestureDetector(
              child: Image.asset("user.png", height: 50, width: 50),
              onTap: () {
                var route =
                    MaterialPageRoute(builder: (context) => ProfilePage());
                Navigator.of(context).push(route);
              },
            )
          ]),
        ),
        body: Container(
          child: _Body(),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("logo_completo.png"),
                  colorFilter: ColorFilter.mode(
                      Colors.white.withOpacity(0.4), BlendMode.dstATop))),
        ));
  }
}

class _DrawerTile extends StatefulWidget {
  final bool current;
  final String img;
  final String text;

  _DrawerTile({required this.current, required this.img, required this.text});

  @override
  State<_DrawerTile> createState() => _DrawerTileState();
}

class _DrawerTileState extends State<_DrawerTile> {
  @override
  Widget build(BuildContext context) {
    Color tileColor = Color(0xAADDDDFF);
    if (widget.current) {
      tileColor = Color(0xFF9999FF);
    }

    var categoryRoute;
    switch (widget.text) {
      case "Categorias":
        categoryRoute = MaterialPageRoute(builder: (context) => CategoryPage());
        break;
      case "Usuarios":
        categoryRoute = MaterialPageRoute(builder: (context) => UserPage());
        break;
      case "Estadisticas":
        categoryRoute = MaterialPageRoute(builder: (context) => StatsPage());
        break;
      case "Denuncias":
        categoryRoute =
            MaterialPageRoute(builder: (context) => ComplaintsPage());
        break;
    }

    return Material(
      child: ListTile(
        enabled: true,
        tileColor: tileColor,
        hoverColor: Color(0xAAAAAAFF),
        title: Row(
          children: [
            Image.asset(
              widget.img,
              height: 25,
              width: 25,
            ),
            const Spacer(),
            Text(widget.text),
            const Spacer(flex: 40)
          ],
        ),
        onTap: () {
          Navigator.of(context).push(categoryRoute);
        },
      ),
    );
  }
}

class _Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(
      children: [
        Row(
          children: [
            Spacer(),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
              padding: EdgeInsets.fromLTRB(30, 0, 0, 0),
              height: 40,
              width: 500,
              decoration: BoxDecoration(
                  color: Color(0x660000aa),
                  borderRadius: BorderRadius.circular(90)),
              child: Row(
                children: [
                  Image.asset(
                    "search.png",
                    height: 20,
                  ),
                  Spacer(flex: 5),
                  Text("Buscar informacion"),
                  Spacer(flex: 90)
                ],
              ),
            )
          ],
        ),
        Spacer(),
        Row(
          children: [
            Spacer(),
            Container(
              child: Column(children: [
                Container(
                  alignment: Alignment.center,
                  color: Color(0x660000AA),
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  width: 400,
                  child: Text("Usuarios mas activos",
                      style: TextStyle(fontSize: 30)),
                ),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Nereida", style: TextStyle(fontSize: 25))),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Alfonsiete", style: TextStyle(fontSize: 25))),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Juan", style: TextStyle(fontSize: 25))),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Valentimo", style: TextStyle(fontSize: 25))),
              ]),
            ),
            Spacer(),
            Container(
              child: Column(children: [
                Container(
                  alignment: Alignment.center,
                  color: Color(0x660000AA),
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  width: 400,
                  child: Text("Usuarios mejor valorados",
                      style: TextStyle(fontSize: 30)),
                ),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Juan", style: TextStyle(fontSize: 25))),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Marcosete", style: TextStyle(fontSize: 25))),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("El Pere", style: TextStyle(fontSize: 25))),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Cristi33", style: TextStyle(fontSize: 25))),
              ]),
            ),
            Spacer()
          ],
        ),
        Spacer(),
        Row(
          children: [
            Spacer(),
            Container(
              child: Column(children: [
                Container(
                  alignment: Alignment.center,
                  color: Color(0x660000AA),
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  width: 400,
                  child: Text("Categorias mas activas",
                      style: TextStyle(fontSize: 30)),
                ),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Ropa", style: TextStyle(fontSize: 25))),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Deportes", style: TextStyle(fontSize: 25))),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Vehiculos", style: TextStyle(fontSize: 25))),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Hogar", style: TextStyle(fontSize: 25))),
              ]),
            ),
            Spacer(),
            Container(
              child: Column(children: [
                Container(
                  alignment: Alignment.center,
                  color: Color(0x660000AA),
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  width: 400,
                  child: Text("Categorias mas populares",
                      style: TextStyle(fontSize: 30)),
                ),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Mascotas", style: TextStyle(fontSize: 25))),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Ropa", style: TextStyle(fontSize: 25))),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Deportes", style: TextStyle(fontSize: 25))),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    width: 400,
                    decoration: BoxDecoration(
                        color: Color(0xaaddddff),
                        border: Border.all(color: Color(0xFFDDDDFF))),
                    child: Text("Vehiculos", style: TextStyle(fontSize: 25))),
              ]),
            ),
            Spacer()
          ],
        ),
        Spacer()
      ],
    ));
  }
}
