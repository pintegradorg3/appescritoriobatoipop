import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:p_i_escritorio_empleados/pages/complaints_page.dart';
import 'package:p_i_escritorio_empleados/pages/profile_page.dart';
import 'package:p_i_escritorio_empleados/pages/stats_page.dart';
import 'category_page.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(children: [const Spacer(), _Body(), const Spacer()]),
      backgroundColor: const Color(0x205E60CE),
    );
  }
}

class _Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool _passwordVisible = false;
    return Center(
        child: Container(
      width: 400,
      height: 500,
      color: Colors.white,
      padding: const EdgeInsets.fromLTRB(20, 50, 20, 50),
      child: Column(
        children: [
          Image.asset("logo_completo.png",
              alignment: Alignment.center, height: 100, width: 300),
          Spacer(),
          const Text(
            "Email",
            textAlign: TextAlign.left,
          ),
          Spacer(),
          TextFormField(
              decoration: InputDecoration(
            labelText: "Email",
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: const BorderSide(),
            ),
          )),
          const Spacer(),
          const Text("Password"),
          const Spacer(),
          TextFormField(
            keyboardType: TextInputType.text,
            // controller: _userPasswordController,
            obscureText: !_passwordVisible, //This will obscure text dynamically
            decoration: InputDecoration(
              labelText: 'Password',
              hintText: 'Enter your password',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: const BorderSide(),
              ),
              // Here is key idea
              suffixIcon: IconButton(
                icon: Icon(
                  // Based on passwordVisible state choose the icon
                  _passwordVisible ? Icons.visibility : Icons.visibility_off,
                  color: Theme.of(context).primaryColorDark,
                ),
                onPressed: () {
                  _passwordVisible = !_passwordVisible;
                },
              ),
            ),
          ),
          const Spacer(),
          GestureDetector(
              child: Container(
                width: 400,
                height: 50,
                decoration: BoxDecoration(
                  color: const Color(0xFF5A2CA8),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: const Align(
                  alignment: Alignment.center,
                  child: Text(
                    "Iniciar sesion",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
              onTap: () {
                var route =
                    MaterialPageRoute(builder: (context) => CategoryPage());
                Navigator.of(context).push(route);
              }),
        ],
      ),
    ));
  }
}
