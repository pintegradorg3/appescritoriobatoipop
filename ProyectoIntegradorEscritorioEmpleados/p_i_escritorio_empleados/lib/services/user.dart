// // To parse this JSON data, do
// //
// //     final user = userFromJson(jsonString);
//
// import 'dart:convert';
//
// User userFromJson(String str) => User.fromJson(json.decode(str));
//
// String userToJson(User data) => json.encode(data.toJson());
//
// class User {
//   User({
//     this.contrasenyaUsuario,
//     this.telefonoUsuario,
//     this.nombreUsuario,
//     this.apellidosUsuario,
//     this.correoUsuario,
//     this.codigoPostal,
//     this.direccionUsuario,
//     this.poblacion,
//     this.name,
//   });
//
//   String contrasenyaUsuario;
//   String telefonoUsuario;
//   String nombreUsuario;
//   String apellidosUsuario;
//   String correoUsuario;
//   String codigoPostal;
//   String direccionUsuario;
//   String poblacion;
//   String name;
//
//   factory User.fromJson(Map<String, dynamic> json) => User(
//         contrasenyaUsuario: json["contrasenyaUsuario"],
//         telefonoUsuario: json["telefonoUsuario"],
//         nombreUsuario: json["nombreUsuario"],
//         apellidosUsuario: json["apellidosUsuario"],
//         correoUsuario: json["correoUsuario"],
//         codigoPostal: json["codigoPostal"],
//         direccionUsuario: json["direccionUsuario"],
//         poblacion: json["poblacion"],
//         name: json["name"],
//       );
//
//   Map<String, dynamic> toJson() => {
//         "contrasenyaUsuario": contrasenyaUsuario,
//         "telefonoUsuario": telefonoUsuario,
//         "nombreUsuario": nombreUsuario,
//         "apellidosUsuario": apellidosUsuario,
//         "correoUsuario": correoUsuario,
//         "codigoPostal": codigoPostal,
//         "direccionUsuario": direccionUsuario,
//         "poblacion": poblacion,
//         "name": name,
//       };
// }
