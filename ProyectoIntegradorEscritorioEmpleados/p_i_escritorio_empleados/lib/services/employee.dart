// // To parse this JSON data, do
// //
// //     final employee = employeeFromJson(jsonString);
//
// import 'dart:convert';
//
// List<Employee> employeeFromJson(String str) =>
//     List<Employee>.from(json.decode(str).map((x) => Employee.fromJson(x)));
//
// String employeeToJson(List<Employee> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));
//
// class Employee {
//   Employee({
//     this.id,
//     this.name,
//     this.apellidosEmpleado,
//     this.contrasenyaEmpleado,
//     this.telefonoEmpleado,
//     this.correoEmpleado,
//     this.direccionEmpleado,
//     this.poblacion,
//   });
//
//   int id;
//   String name;
//   String apellidosEmpleado;
//   String contrasenyaEmpleado;
//   String telefonoEmpleado;
//   String correoEmpleado;
//   String direccionEmpleado;
//   String poblacion;
//
//   factory Employee.fromJson(Map<String, dynamic> json) => Employee(
//         id: json["id"],
//         name: json["name"],
//         apellidosEmpleado: json["apellidosEmpleado"],
//         contrasenyaEmpleado: json["contrasenyaEmpleado"],
//         telefonoEmpleado: json["telefonoEmpleado"],
//         correoEmpleado: json["correoEmpleado"],
//         direccionEmpleado: json["direccionEmpleado"],
//         poblacion: json["poblacion"],
//       );
//
//   Map<String, dynamic> toJson() => {
//         "id": id,
//         "name": name,
//         "apellidosEmpleado": apellidosEmpleado,
//         "contrasenyaEmpleado": contrasenyaEmpleado,
//         "telefonoEmpleado": telefonoEmpleado,
//         "correoEmpleado": correoEmpleado,
//         "direccionEmpleado": direccionEmpleado,
//         "poblacion": poblacion,
//       };
// }
