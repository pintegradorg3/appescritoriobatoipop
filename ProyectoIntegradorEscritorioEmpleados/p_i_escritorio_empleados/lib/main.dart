import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:p_i_escritorio_empleados/pages/login_page.dart';

void main() {
  debugPrintHitTestResults = true;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Desktop project app',
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
    );
  }
}
