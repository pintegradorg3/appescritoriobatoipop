package com.example.proyectointegradorempleados;

import com.example.proyectointegradorempleados.pojos.BatoipopEmpleados;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalDate;

public class MenuInicialController {
    @FXML
    private Button articulos;

    @FXML
    private Button categoria;

    @FXML
    private ImageView imagenLogo;


    @FXML
    private Label etiquetaFecha;

    @FXML
    private Button usuarios;

    BatoipopEmpleados empleado;

    @FXML
    public void initialize() {
        this.imagenLogo.setImage(new Image(LoginControllerController.RUTA_LOGO));
        this.etiquetaFecha.setText(String.valueOf(LocalDate.now()));

    }

    public void setEmpleado(BatoipopEmpleados empleado) {
        this.empleado = empleado;
    }

    @FXML
    void mostrarArticulos(ActionEvent event) {

        try {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("MostrarArticulos.fxml"));
            Parent root = loader.load();
            ArticulosController controller = loader.getController();
            controller.setEmpleado(empleado);

            Scene scene = new Scene(root);
            stage.setScene(scene);

            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void mostrarCategoria(ActionEvent event) {
        try {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("MostrarCategorias.fxml"));
            Parent root = loader.load();
            MostrarCategorias controller = loader.getController();
            controller.setEmpleado(empleado);

            Scene scene = new Scene(root);
            stage.setScene(scene);

            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void mostrarUsuarios(ActionEvent event) {

        try {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("PanelUsuario.fxml"));
            Parent root =loader.load();
            PanelUsuarioController controller= loader.getController();
            controller.setEmpleado(empleado);

            Scene scene = new Scene(root);
            stage.setScene(scene);

            stage.showAndWait();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

