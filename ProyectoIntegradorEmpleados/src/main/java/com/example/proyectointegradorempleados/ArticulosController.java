package com.example.proyectointegradorempleados;

import com.example.proyectointegradorempleados.pojos.BatoipopArticles;
import com.example.proyectointegradorempleados.pojos.BatoipopEmpleados;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

public class ArticulosController {

    @FXML
    private Button btonActualizarUsario;

    @FXML
    private Button btonBorrarUsaurio;

    @FXML
    private ImageView imagenIconoSuperior;

    @FXML
    private ImageView imagenUsuario;
    @FXML
    private Text informarUsuario;

    @FXML
    private Label nombreUsuario;
    @FXML
    private TableColumn<BatoipopArticles, String> tDescripcion;

    @FXML
    private TableColumn<BatoipopArticles, Boolean> tDisponible;

    @FXML
    private TableColumn<BatoipopArticles, Integer> tId;

    @FXML
    private TableColumn<BatoipopArticles, String> tNombre;

    @FXML
    private TableColumn<BatoipopArticles, Double> tPrecio;

    @FXML
    private TableView<BatoipopArticles> tableView;

    private Service service = new Service();
    private BatoipopEmpleados empleado=new BatoipopEmpleados();
    private BatoipopArticles articulos =new BatoipopArticles();
    private final ObservableList<BatoipopArticles> MyData = FXCollections.observableArrayList();
    public static final String RUTA_LOGO = "C:..\\ProyectoIntegradorEmpleados\\logocompleto.png";


    @FXML
    public void initialize() {
        try {

            imagenIconoSuperior.setImage(new Image(RUTA_LOGO));
            tId.setCellValueFactory(new PropertyValueFactory<BatoipopArticles, Integer>("id"));
            tNombre.setCellValueFactory(new PropertyValueFactory<BatoipopArticles, String>("name"));
            tPrecio.setCellValueFactory(new PropertyValueFactory<BatoipopArticles, Double>("precioArticulo"));
            tDescripcion.setCellValueFactory(new PropertyValueFactory<BatoipopArticles, String>("descripcionArticulo"));
            tDisponible.setCellValueFactory(new PropertyValueFactory<BatoipopArticles, Boolean>("disponible"));

            tableView.setItems(MyData);
            MyData.addAll(service.buscarTodosArticle());
            tableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<BatoipopArticles>() {
                @Override
                public void changed(ObservableValue<? extends BatoipopArticles> observableValue, BatoipopArticles batoipopArticles, BatoipopArticles t1) {
                    articulos=t1;

                }
            });
            tableView.setVisible(true);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    public void setEmpleado(BatoipopEmpleados empleado) {
        this.empleado = empleado;
        nombreUsuario.setText(empleado.getName()+" "+empleado.getApellidosEmpleado());
        imagenUsuario.setImage(new Image("https://i0.wp.com/public21.es/wp-content/uploads/2016/10/foto-perfil-redonda.png?fit=2363%2C1903&ssl=1"));

    }
    @FXML
    void ActualizarUsuario(ActionEvent event) {

    }

    @FXML
    void borrarUsuario(ActionEvent event) {
        try {

            service.borrarArticle(articulos);
            tableView.getItems().clear();
            tableView.getItems().addAll(service.buscarTodosArticle());

        } catch (Exception e) {
            e.printStackTrace();
        }

        informarUsuario.setText("Se ha borrado el usuario ");
    }

}
