package com.example.proyectointegradorempleados;

import com.example.proyectointegradorempleados.pojos.BatoipopEmpleados;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.IOException;


public class LoginControllerController {
    @FXML
    private Pane panelInicial;

    @FXML
    private ImageView imagenLogo;

    @FXML
    private TextField textFieldEmail;

    @FXML
    private Label labelAviso;

    @FXML
    private TextField textFieldPassword;

    private Service service = new Service();

    public static final String RUTA_LOGO = "C:..\\ProyectoIntegradorEmpleados\\logocompleto.png";

    @FXML
    public void initialize() {
        this.imagenLogo.setImage(new Image(RUTA_LOGO));this.labelAviso.setVisible(false);
    }

    @FXML
    void entrarEnlaApp(ActionEvent event) throws Exception {
        labelAviso.setVisible(false);

        String contrasenya = textFieldPassword.getText();
        String correo = textFieldEmail.getText();

        if (contrasenya == null){
            labelAviso.setText("Contraseña incorrecta");
            labelAviso.setVisible(true);
        }
        if (correo == null ){
            labelAviso.setText("Correo introducido no válido");
            labelAviso.setVisible(true);
        }

        if (contrasenya != null && correo != null){

            BatoipopEmpleados b = service.loginEmpleado(contrasenya, correo);
            if (b!= null){

               cambiarScene(service.loginEmpleado(contrasenya,correo));
            } else {
                labelAviso.setText("Error");
                labelAviso.setVisible(true);
            }
        }
    }

    void cambiarScene(BatoipopEmpleados empleado) {
        try {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("MenuInicial.fxml"));
            Parent root = loader.load();
            MenuInicialController controller = loader.getController();
            controller.setEmpleado(empleado);

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.showAndWait();

        } catch (IOException e) {
            labelAviso.setText("Error, ha surgido un error inesperado");
            labelAviso.setVisible(true);

        }
    }

    @FXML
    void olvidarUsuario(ActionEvent event) {
        AvisoSupervisorController avisoSupervisorController;
        try {
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("AvisoSupervisor.fxml"));
            Parent root = loader.load();
            avisoSupervisorController = loader.getController();

            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.showAndWait();


        } catch (IOException e) {
            labelAviso.setText("Error, ha surgido un error inesperado");
            labelAviso.setVisible(true);
        }
    }

}

