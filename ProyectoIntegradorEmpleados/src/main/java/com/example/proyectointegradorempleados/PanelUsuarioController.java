package com.example.proyectointegradorempleados;

import com.example.proyectointegradorempleados.pojos.BatoipopEmpleados;
import com.example.proyectointegradorempleados.pojos.BatoipopUsuario;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;

public class PanelUsuarioController {
    @FXML
    private Text informarUsuario;

    @FXML
    private ImageView  imagenIconoSuperior, imagenUsuario;

    @FXML
    private Label nombreUsuario;
    @FXML
    private TextField barraBusqueda;
    @FXML
    private Button btanyadiUsuario;
    @FXML
    private Button btonActualizarUsario;
    @FXML
    private Button btonBorrarUsaurio;
    @FXML
    private TableView<BatoipopUsuario> tableView;
    @FXML
    private TableColumn<BatoipopUsuario, String> tApellidos;
    @FXML
    private TableColumn<BatoipopUsuario, String> tCorreo;
    @FXML
    private TableColumn<BatoipopUsuario, Integer> tId;
    @FXML
    private TableColumn<BatoipopUsuario, String> tNombre;
    @FXML
    private TableColumn<BatoipopUsuario, String> tTelefono;


    private BatoipopEmpleados empleado = new BatoipopEmpleados();
    private BatoipopUsuario usuario = new BatoipopUsuario();
    private Service service = new Service();
    private final ObservableList<BatoipopUsuario>MyData = FXCollections.observableArrayList();


    public PanelUsuarioController(){}

    @FXML
    public void initialize()  {
        try {
            this.imagenIconoSuperior.setImage(new Image(LoginControllerController.RUTA_LOGO));

            tId.setCellValueFactory(new PropertyValueFactory<BatoipopUsuario, Integer>("id"));
            tNombre.setCellValueFactory(new PropertyValueFactory<BatoipopUsuario, String>("name"));
            tApellidos.setCellValueFactory(new PropertyValueFactory<BatoipopUsuario, String>("apellidosUsuario"));
            tTelefono.setCellValueFactory(new PropertyValueFactory<BatoipopUsuario, String>("telefonoUsuario"));
            tCorreo.setCellValueFactory(new PropertyValueFactory<BatoipopUsuario, String>("correoUsuario"));

            tableView.setItems(MyData);

            MyData.addAll(service.buscarTodosUsuarios());
            tableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<BatoipopUsuario>() {
                @Override
                public void changed(ObservableValue<? extends BatoipopUsuario> observableValue, BatoipopUsuario batoipopUsuario, BatoipopUsuario t1) {
                    usuario=t1;

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    @FXML
    void ActualizarUsuario(ActionEvent event) {
        try {


            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ActualizarUsuario.fxml"));
            Parent root = loader.load();
            AnyadirUsuarioController controller = loader.getController();

            controller.metodoVacio(usuario);

            Scene scene = new Scene(root);
            stage.setScene(scene);

            stage.showAndWait();
            informacionActualizada();
            informarUsuario.setText("El Usuario Ha sido actualizado");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void obtenerDato(BatoipopUsuario usuario){

        this.usuario = usuario;
    }

    @FXML
    void borrarUsuario(ActionEvent event) {
        try {

            service.borrarUsuario(usuario);
            tableView.getItems().clear();
            tableView.getItems().addAll(service.buscarTodosUsuarios());

        } catch (Exception e) {
            e.printStackTrace();
        }

        informarUsuario.setText("Se ha borrado el usuario ");
    }

    void informacionActualizada() {
        try {

            service.actualizarUsuario(usuario);

            tableView.getItems().clear();
            tableView.getItems().addAll(service.buscarTodosUsuarios());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void setEmpleado(BatoipopEmpleados empleado) {
        this.empleado = empleado;
        nombreUsuario.setText(empleado.getName()+" "+empleado.getApellidosEmpleado());
        imagenUsuario.setImage(new Image("https://i0.wp.com/public21.es/wp-content/uploads/2016/10/foto-perfil-redonda.png?fit=2363%2C1903&ssl=1"));
    }
}
