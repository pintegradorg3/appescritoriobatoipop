package com.example.proyectointegradorempleados;

import com.example.proyectointegradorempleados.pojos.BatoipopCategoria;
import com.example.proyectointegradorempleados.pojos.BatoipopEmpleados;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

public class MostrarCategorias {
    @FXML
    private Button btonActualizarUsario;

    @FXML
    private Button btonBorrarUsaurio;

    @FXML
    private ImageView imagenIconoSuperior;

    @FXML
    private ImageView imagenUsuario;

    @FXML
    private Text informarUsuario;

    @FXML
    private Label nombreUsuario;

    @FXML
    private TableColumn<BatoipopCategoria, String> tDescripcion;

    @FXML
    private TableColumn<BatoipopCategoria, Integer> tId;

    @FXML
    private TableColumn<BatoipopCategoria,String> tNombre;

    @FXML
    private TableView<BatoipopCategoria> tableView;

    private BatoipopEmpleados empleado = new BatoipopEmpleados();
    private BatoipopCategoria categoria = new BatoipopCategoria();
    private Service service = new Service();
    private final ObservableList<BatoipopCategoria> MyData= FXCollections.observableArrayList();


    @FXML
    public void initialize()  {
        try {
            this.imagenIconoSuperior.setImage(new Image(LoginControllerController.RUTA_LOGO));

            tId.setCellValueFactory(new PropertyValueFactory<BatoipopCategoria, Integer>("id"));
            tNombre.setCellValueFactory(new PropertyValueFactory<BatoipopCategoria, String>("name"));
            tDescripcion.setCellValueFactory(new PropertyValueFactory<BatoipopCategoria, String>("descripcionCategoria"));

            tableView.setItems(MyData);

            MyData.addAll(service.buscarTodasCategorias());

            tableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<BatoipopCategoria>() {
                @Override
                public void changed(ObservableValue<? extends BatoipopCategoria> observableValue, BatoipopCategoria batoipopCategoria, BatoipopCategoria t1) {
                    categoria=t1;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    void ActualizarUsuario(ActionEvent event) {

    }

    @FXML
    void borrarUsuario(ActionEvent event) {
        try {

            service.borrarCategoria(categoria);
            tableView.getItems().clear();
            tableView.getItems().addAll(service.buscarTodasCategorias());

        } catch (Exception e) {
            e.printStackTrace();
        }

        informarUsuario.setText("Se ha borrado el usuario ");


    }
    public void setEmpleado(BatoipopEmpleados empleado) {
        this.empleado = empleado;
        nombreUsuario.setText(empleado.getName() + " " + empleado.getApellidosEmpleado());
        imagenUsuario.setImage(new Image("https://i0.wp.com/public21.es/wp-content/uploads/2016/10/foto-perfil-redonda.png?fit=2363%2C1903&ssl=1"));
    }
}
