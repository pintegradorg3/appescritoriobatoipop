package com.example.proyectointegradorempleados;

import com.example.proyectointegradorempleados.dao.UsuarioDAO;
import com.example.proyectointegradorempleados.pojos.BatoipopUsuario;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.List;

public class AnyadirUsuarioController {
    @FXML
    private Button btonActualizar;

    @FXML
    private TextField textApellidos;

    @FXML
    private TextField textCodigoPostal;

    @FXML
    private TextField textCorreo;

    @FXML
    private TextField textDireccion;

    @FXML
    private TextField textNick;

    @FXML
    private TextField textNombreUsuario;

    @FXML
    private TextField textTelefono;

    @FXML
    private TextField textcontrasenya;
    @FXML
    private TextField textoPoblacion;
    @FXML
    private Text errorCampos;
    private Service service = new Service();
    BatoipopUsuario usuario=new BatoipopUsuario();



    void metodoVacio(BatoipopUsuario usuario) {
        this.usuario=usuario;
        obtenerdatos();

    }


    @FXML
    void DevolverDatos(ActionEvent event) {
        try {
            usuario.setApellidosUsuario(textApellidos.getText());
            usuario.setCodigoPostal(textApellidos.getText());
            usuario.setCorreoUsuario(textApellidos.getText());
            usuario.setDireccionUsuario(textApellidos.getText());
            usuario.setName(textApellidos.getText());
            usuario.setNombreUsuario(textApellidos.getText());
            usuario.setTelefonoUsuario(textApellidos.getText());
            usuario.setContrasenyaUsuario(textApellidos.getText());

            service.actualizarUsuario(usuario);
            Node source = (Node) event.getSource();
            Stage stage= (Stage) source.getScene().getWindow();
            stage.close();



        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public boolean comprobante(List<BatoipopUsuario> usuarios, BatoipopUsuario usuario) {
    return false;
    }
    @FXML
    public void initialize() {
      obtenerdatos();
    }
    public void obtenerdatos(){
        textApellidos.setText(usuario.getApellidosUsuario());
        textCodigoPostal.setText(usuario.getCodigoPostal());
        textCorreo.setText(usuario.getCorreoUsuario());
        textDireccion.setText(usuario.getDireccionUsuario());
        textNick.setText(usuario.getName());
        textNombreUsuario.setText(usuario.getNombreUsuario());
        textTelefono.setText(usuario.getTelefonoUsuario());
        textcontrasenya.setText(usuario.getContrasenyaUsuario());
    }
}

