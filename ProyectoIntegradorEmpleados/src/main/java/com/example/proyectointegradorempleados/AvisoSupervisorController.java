package com.example.proyectointegradorempleados;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class AvisoSupervisorController {

    @FXML
    ImageView imgLogo;
    @FXML
    AnchorPane fondo;

    @FXML
    public void initialize() {
        this.imgLogo.setImage(new Image(LoginControllerController.RUTA_LOGO));
    }

}
