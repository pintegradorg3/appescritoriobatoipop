package com.example.proyectointegradorempleados.dao;

import com.google.gson.Gson;
import com.example.proyectointegradorempleados.pojos.BatoipopArticles;
import com.example.proyectointegradorempleados.pojos.BatoipopCategoria;
import com.example.proyectointegradorempleados.pojos.BatoipopUsuario;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArticuloDao implements GenericDao.GenericDAO<BatoipopArticles> {

    final static String TABLA = "batoipop_articles";
    final static String PK = "id";
    final static String HTTP = "http://137.74.226.43:8080/article/";


    public ArticuloDao() throws SQLException {
    }

    public void cerrar() throws SQLException {
    }

    @Override
    public BatoipopArticles findByPK(int id) throws Exception {
        String lin, salida = "";
        BatoipopArticles cliente = null;
        Gson gson;
        URL url;
        HttpURLConnection con;

        try {
            url = new URL(HTTP + id);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
                while ((lin = br.readLine()) != null) {
                    salida = salida.concat(lin);
                }
                gson = new Gson();
                cliente = gson.fromJson(salida, BatoipopArticles.class);

                con.disconnect();
                return cliente;
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        return null;

    }

    @Override // OKI
    public List<BatoipopArticles> findAll() throws Exception {
        String lin,salida="";
        List<BatoipopArticles> listaEmpleados=null;
        BatoipopArticles[] articulos;
        Gson gson;
        URL url;
        HttpURLConnection con;
        try {
            url=new URL(HTTP);
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");

            if (con.getResponseCode()!=HttpURLConnection.HTTP_OK){
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }else {
                BufferedReader br =new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);
                }
                gson=new Gson();
                System.out.println(salida);
                articulos=gson.fromJson(salida,BatoipopArticles[].class);
                listaEmpleados= new ArrayList<>(Arrays.asList(articulos));
                con.disconnect();

            }

        } catch (IOException | RuntimeException e) {
            e.printStackTrace();
        }
        return listaEmpleados;

    }

    @Override // OKI
    public boolean insert(BatoipopArticles t) throws Exception {
        URL url ;
        HttpURLConnection con;
        Gson gson=new Gson();
        String articulo= gson.toJson(t,BatoipopArticles.class);
        System.out.println(articulo);

        try {
            url=new URL(HTTP);
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            OutputStream os =con.getOutputStream();
            os.write(articulo.getBytes());
            os.flush();
            if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                return false;
                //throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }

        } catch (Exception e) {
            return false;
            //e.printStackTrace();
        }

        return true;

    }

    @Override // OKI
    public BatoipopArticles insertGenKey(BatoipopArticles t) throws Exception {
       CategoriaDAO categoriaDAO = new CategoriaDAO();
        UsuarioDAO usuarioDAO = new UsuarioDAO();

        BatoipopCategoria g = categoriaDAO.findByPK(t.getBatoipopCategoria().getId());
        BatoipopUsuario u = usuarioDAO.findByPK(t.getBatoipopUsuarioByPublicarArticulo().getId());

        return null;

    }

    @Override // OKI
    public boolean update(BatoipopArticles t) throws Exception {
        URL url ;
        HttpURLConnection con;
        Gson gson;
        try {
            gson=new Gson();
            String empleadoJson=gson.toJson(t,BatoipopArticles.class);
            url=new URL(HTTP);
            con=(HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            OutputStream os = con.getOutputStream();
            os.write(empleadoJson.getBytes());
            os.flush();

            if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }
            con.disconnect();



        } catch (IOException e) {
            return false;
        }
        return true;

    }

    @Override // OKI
    public boolean delete(int id) throws Exception {
        URL url;
        HttpURLConnection con;

        try {
            url = new URL(HTTP+id );
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }

            con.disconnect();

        } catch (IOException e) {
            return  false;
        }

        return  true;
    }

    @Override
    public boolean delete(BatoipopArticles t) throws Exception {
        return this.delete(t.getId());
    }

    @Override
    public int size() throws Exception {
      //  ResultSet rs = pstCount.executeQuery();
      //  rs.next();
      //  return rs.getInt(1);
        return 0;
    }

    @Override
    public boolean exists(int id) throws Exception {
        return findByPK(id) != null;
    }

    @Override // IGNORANDO A MUERTE
    public List<BatoipopArticles> findByExample(BatoipopArticles t) throws Exception {
        return null;
    }
}
