package com.example.proyectointegradorempleados.dao;

import com.google.gson.Gson;
import com.example.proyectointegradorempleados.pojos.BatoipopCategoria;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoriaDAO implements GenericDao.GenericDAO<BatoipopCategoria> {
    final static String HTTP = "http://137.74.226.43:8080/categoria/";



    public CategoriaDAO() throws SQLException {
       // Connection con = ConexionBD.getConexion();
    }



    @Override // OKI
    public BatoipopCategoria findByPK(int id) throws Exception {
        String lin,salida="";
        List<BatoipopCategoria> listaEmpleados = null;
        BatoipopCategoria categoria = null;
        Gson gson;
        URL url;
        HttpURLConnection con;
        try {
            url=new URL(HTTP+id);
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");

            if (con.getResponseCode()!=HttpURLConnection.HTTP_OK){
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }else {
                BufferedReader br=new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);
                }
                gson=new Gson();
                categoria=gson.fromJson(salida, BatoipopCategoria.class);
                con.disconnect();

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return categoria;

    }

    @Override // OKI
    public List<BatoipopCategoria> findAll() throws Exception {
        String lin,salida="";
        List<BatoipopCategoria> listaEmpleados=null;
        BatoipopCategoria[] categoria;
        Gson gson;
        URL url;
        HttpURLConnection con;
        try {
            url=new URL(HTTP);
            con=(HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept","application/json");
            if (con.getResponseCode()!=HttpURLConnection.HTTP_OK){
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }else {
                BufferedReader br =new BufferedReader(new InputStreamReader(con.getInputStream()));
                while ((lin=br.readLine())!=null){
                    salida=salida.concat(lin);
                }
                gson=new Gson();
                categoria=gson.fromJson(salida,BatoipopCategoria[].class);
                listaEmpleados=new ArrayList<BatoipopCategoria>();
                listaEmpleados= Arrays.asList(categoria);
                con.disconnect();

            }

        } catch (IOException | RuntimeException e) {
            e.printStackTrace();
        }
        return listaEmpleados;

    }

    @Override
    public boolean insert(BatoipopCategoria t) throws Exception {
        URL url ;
        HttpURLConnection con;
        Gson gson=new Gson();
        String empleadoJson= gson.toJson(t, BatoipopCategoria.class);

        try {
            url=new URL(HTTP);
            con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");

            OutputStream os =con.getOutputStream();
            os.write(empleadoJson.getBytes());
            os.flush();
            if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override //Hacer ultimo
    public BatoipopCategoria insertGenKey(BatoipopCategoria t) throws Exception {

     /*   pstInsertKey.setString(1, t.getDescripcionCategoria());
        pstInsertKey.setString(2, t.getName());

        int insertados = pstInsertKey.executeUpdate();
        if(insertados == 1){
            ResultSet resultSet = pstInsertKey.getGeneratedKeys();
            resultSet.next();
            t.setId(resultSet.getInt(1));
            return t;
        } return null;*/
        return null;
    }

    @Override
    public boolean update(BatoipopCategoria t) throws Exception {
        URL url ;
        HttpURLConnection con;
        Gson gson;
        try {
            gson=new Gson();
            String empleadoJson=gson.toJson(t,BatoipopCategoria.class);
            url = new URL(HTTP);
            con=(HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            OutputStream os = con.getOutputStream();
            os.write(empleadoJson.getBytes());
            os.flush();

            if (con.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }
            con.disconnect();

        } catch (IOException e) {
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(int id) throws Exception {
       /* pstDelete.setInt(1, id);
        int borrados = pstDelete.executeUpdate();
        return (borrados == 1);*/
        URL url;
        HttpURLConnection con;

        try {
            url = new URL(HTTP+id );
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");
            con.setRequestProperty("Accept", "application/json");

            if (con.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Failed : HTTP error code : " + con.getResponseCode());
            }

            con.disconnect();

        } catch (IOException e) {
            return  false;
        }

        return  true;
    }

    @Override
    public boolean delete(BatoipopCategoria t) throws Exception {
        return this.delete(t.getId());
    }

    @Override
    public int size() throws Exception {
     /*   ResultSet rs = pstCount.executeQuery();
        rs.next();
        return rs.getInt(1);*/
        return 0;
    }

    @Override
    public boolean exists(int id) throws Exception {
        return findByPK(id) != null;

    }

    @Override  // IGNORANDO A MUERTE
    public List<BatoipopCategoria> findByExample(BatoipopCategoria t) throws Exception {
        return null;
    }
}
