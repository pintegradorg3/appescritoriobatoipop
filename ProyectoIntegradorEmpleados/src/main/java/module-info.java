module com.example.proyectointegradorempleados {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.google.gson;

    opens com.example.proyectointegradorempleados.pojos to com.google.gson;
    opens com.example.proyectointegradorempleados to javafx.fxml;
    exports com.example.proyectointegradorempleados;
    exports com.example.proyectointegradorempleados.pojos;
}